#!/bin/bash

#update index files for images based on the template
ls -d /opt/mean.js/public/img | while read zz
do 
  cd "$zz"
  ls -f *.jpg| grep -v icon.jpg  | grep -v icon2.jpg | while read -r i
  do j=$(echo "$i" | sed 's/\.jpg//')
     cat /opt/mean.js/public/template.html | sed "s/IMAGE/$j/" > "$j.html"
  done
  cd /opt/mean.js/public 
done 
