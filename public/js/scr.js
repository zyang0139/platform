// initialize display
var DivId = "im1";

function getQueryParams(qs) {
    qs = qs.split('+').join(' ');

    var params = {},
        tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
    }

    return params;
}



function loadIm() {
  console.log("Inside loadIm function from scr.js");
  var divObj = document.getElementById("im1");
  
  //Log by Rayhan
  var displayImUrl = document.getElementById("im1im").src;
  var fileNameIndex = displayImUrl.lastIndexOf(":3000/") + 6;
  var filename = displayImUrl.substr(fileNameIndex);
  console.log("File loaded: ", filename);

  var xhr = new XMLHttpRequest();
  var url = 'http://localhost:3000/api/logs/';
  xhr.open('POST', url, true);
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  var image_path = filename;
  queryStr = 'image=' + image_path;
  //queryStr = 'name=' + name + '&value=' + value; 
  xhr.responseType = 'json';
  xhr.send(queryStr);
  xhr.onreadystatechange = function() {
    if (xhr.readyState == XMLHttpRequest.DONE) {
      console.log("Tag-Log created successfully");
    }
  };

  // display new image
  divObj.style.display = "block";
  
  query = getQueryParams(document.location.search);
  if ( query.hasOwnProperty("rect"))
  {
    anno.addPlugin('PolygonSelector', { activate: false });
    anno.addPlugin('SemanticTagging', { endpoint_url: 'http://samos.mminf.univie.ac.at:8080/wikipediaminer' });
  }
  else
  {

	anno.addPlugin('PolygonSelector', { activate: true });
	anno.addPlugin('SemanticTagging', { endpoint_url: 'http://samos.mminf.univie.ac.at:8080/wikipediaminer' });
  }
  anno.makeAnnotatable (document.getElementById("im1im"));
  // tag persistence
  var xhr = new XMLHttpRequest();
  var displayImUrl = document.getElementById("im1im").src;
  //Changed the URL
  var url = 'http://localhost:3000/api/tags/image/';
  var fileNameIndex = displayImUrl.lastIndexOf(":3000/") + 6;
  var filename = displayImUrl.substr(fileNameIndex);
  console.log("File loaded: ", filename);
  filename = filename .replace(/\//g,'%2F');
  var url = url + filename; 
  xhr.open('GET', url, true);
  xhr.responseType = 'json';
  xhr.onreadystatechange = function() {
    if (xhr.readyState == XMLHttpRequest.DONE) {
      // send array of all tag-objects for drawing
      if( Object.prototype.toString.call( xhr.response ) === '[object Array]' ) {
         showTags(xhr.response);
      }
    }
  };
  xhr.open('GET', url, true);
  xhr.send();

  //Log by Rayhan
  console.log("Loaded url:" + url);
}

// draw pre-existing tags on currently displayed image
function showTags(tagsArray) {
  console.log("Tags:", tagsArray);
  var ntags = tagsArray.length ? tagsArray.length: 0;
  // find tags of currently displayed image
  for(var i=0; i<ntags; i++) {
    var tagObj = tagsArray[i];
    if(tagObj.image.includes("Photo/")) {
      tagObj.image = tagObj.image.replace("Photo", "Photos");
    }
    var displayImUrl = document.getElementById("im1im").src;
    console.log(tagObj.image);
    console.log(decodeURI(displayImUrl));
    //if(tagObj.image==decodeURI(displayImUrl)) {
      console.log("tagObj.image= ", tagObj.image);
      console.log(decodeURI(displayImUrl));
      // construct annotation
      var annotation = {
        src : displayImUrl,
        text : tagObj.tag,
        shapes : JSON.parse(tagObj.location),
        //add id to tag object
        id : tagObj._id
      };
      // draw
      anno.addAnnotation(annotation);
    //}
  }
}


/* Don't really need this...
// callback for showing tags below images
anno.addHandler('onAnnotationCreated', function(annotation) {
var newTag = annotation.text;
var pObj = document.getElementById(DivId.concat("p"));
var oldTag = pObj.innerHTML;
pObj.innerHTML = oldTag.concat(newTag,"; ");
});
*/

// callback for adding a tag
anno.addHandler('onAnnotationCreated', function(annotation) {
  console.log('Log for annotation by Rayhan');
  console.log(annotation);
  var xhr = new XMLHttpRequest();
  var url = 'http://localhost:3000/api/tags/';
  xhr.open('POST', url, true);
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

  // added for tag suggestion on typing
  var tag_text = '';
  annotation.tags.forEach(function(tag) {
    if(tag.status && tag.status == 'rejected'){
      // do nothing- rejected
    }else{
      if(tag_text !== '') tag_text = tag_text + ';';
      tag_text = tag_text + tag.title;
    }
  });
  console.log('tag_text:', tag_text);
  image_name = annotation.src.replace(/.*3000\//, "");
  queryStr = 'tag=' + tag_text + '&location=' + JSON.stringify(annotation.shapes) + '&image=' + image_name + '&id=' + annotation.id; 
  xhr.responseType = 'json';
  xhr.send(queryStr);
  xhr.onreadystatechange = function() {
    var displayImUrl = document.getElementById("im1im").src;
    if (xhr.readyState == XMLHttpRequest.DONE) {
      //Building the tag. Trying to get delete to work on currently made tags 
      var annotation = {
        id : xhr.response._id,
        src : displayImUrl,
        text : xhr.response.tag,
        shapes : JSON.parse(xhr.response.location)
      };
      //Trying to force tag.id onto annotation
      anno.addAnnotation(annotation);
    }
  };
  //document.location.reload();
});
//////////////////////////////////
// Delete 
anno.addHandler('onAnnotationRemoved', function(annotation) {
  anno.removeAnnotation(annotation);
  var xhr = new XMLHttpRequest();
  var url = 'http://localhost:3000/api/tags/';
  var XXX = annotation.id;
  url = url + XXX;	
  console.log(url);
  xhr.open('DELETE', url, true);
  xhr.send(url);
  document.location.reload();
});
///////////////////////////////////
//Edit
anno.addHandler('onAnnotationUpdated', function(annotation) {
  var xhr = new XMLHttpRequest();
  var url = 'http://localhost:3000/api/tags/';
  var XXX = annotation.id;
  url = url + XXX;	
  xhr.open('PUT', url, true);
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

  // added for tag suggestion on typing
  var tag_text = '';
  annotation.tags.forEach(function(tag) {
    if(tag.status && tag.status == 'rejected'){
      // do nothing- rejected
    }else{
      if(tag_text !== '') tag_text = tag_text + ';';
      tag_text = tag_text + tag.title;
    }
  });
  console.log('tag_text:', tag_text);
  image_name2 = annotation.src.replace(/.*3000\//, "");
  queryStr = 'tag=' + tag_text + '&location=' + JSON.stringify(annotation.shapes) + '&image=' + image_name2 + '&id=' + annotation.id; 
  xhr.send(queryStr);
  var displayImUrl = document.getElementById("im1im").src;
  if (xhr.readyState == XMLHttpRequest.DONE) {
    //Trying to get edit to work on all tags 
    var annotation = {
      id : xhr.response._id,
      src : displayImUrl,
      text : xhr.response.tag,
      shapes : JSON.parse(xhr.response.location)
    };
    anno.addAnnotation(annotation);
  }
  document.location.reload();
});


