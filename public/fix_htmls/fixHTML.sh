#!/bin/bash

str1='<link type="text/css" rel="stylesheet" href="/css/semantic-tagging-plugin.css" />'
str2='<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>'
str3='<script type="text/javascript" src="/js/semantic-tagging-plugin.js"></script>'
str4='<script type="text/javascript" src="/js/editor-extension.js"></script>'
str5='<script type="text/javascript" src="/js/popup-extension.js"></script>'

ls -d 201[1-6]/UT100-11D/Daily\ Photos/ | while read zz
do
	ls "$zz" | grep "html" | while read f
	do
		if ! cat "$zz/$f" | grep "icon" > /dev/null
		then 
			if ! cat image.html | grep "semantic-tagging-plugin" > /dev/null
				then 
					sed	"6 a $str1" image.html
					sed "8 a $str2" image.html
					sed "11 a $str3" image.html
					sed "12 a $str4" image.html
					sed "13 a $str5" image.html
			fi
		fi
	done
done
