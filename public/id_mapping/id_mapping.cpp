#include <iostream>
#include <fstream>
#include <cstdio>
#include <map>
#include <cstdlib>
#include <string>

using namespace std;

int main(int argc, char** argv) {
	string oldId, newId, searchId;
	ifstream oldIn, newIn;
	ofstream mapOut;
	map<string, string> idMap;
	map<string, string>::iterator foundId;
	
	if(argc < 3) {
		perror("Usage: ./id_mapping \"old id's file\" \"new id's file\"\n");
		exit(1);
	}
	
	oldIn.open(argv[1]);
	newIn.open(argv[2]);

	mapOut.open("mappings.txt");

	while(oldIn >> oldId) {
		newIn >> newId;
		idMap.insert(make_pair(oldId, newId));
		mapOut << oldId << " = " << newId << endl;
	}

	cout << "Enter UTID: ";
	cin >> searchId;

	foundId = idMap.find(searchId);
	if(foundId != idMap.end()) {
		cout << endl << "New ID: " << foundId->second << endl;
	}

	return 0;
}
