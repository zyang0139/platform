#!/bin/bash

index=1
ls -d 201[1-7]/*/Daily\ Photo*/ |  while read zz; do	
  oldId=$(echo "$zz" | cut -d'/' -f2)
  startYear=$(echo "$zz" | cut -d'/' -f1 | grep -o '..$')
  newId=$(cat newIds.txt | sed -n ''$index'p')
  echo $oldId $newId
  mkdir -p img/$newId
  cd img/$newId
  index=$((index+1))
  
  ls "../../$zz" | grep -v "icon" | grep ".JPG" | while read p; do
    utid=$(echo "$p" | sed 's/_.*//') 
    month=$(echo "$p" | cut -d'_' -f2)
    day=$(echo "$p" | cut -d'_' -f3)

    if ! ( echo "$month" | grep "0" > /dev/null )
      then
        [[ $month -lt 10 ]] && month="0$month"
      fi
  
      if ! ( echo "$day" | grep "0" > /dev/null )
      then
        [[ $day -lt 10 ]] && day="0$day"
    fi
	
		if echo "$p"  | grep '(' > /dev/null 
		then
			if echo "$p" | grep " " > /dev/null
			then 
				year=$(echo "$p" |  sed 's/.*_//' | cut -d ' ' -f1 | grep -o '..$')
			else
				year=$(echo "$p" | sed 's/.*_//' | cut -d '(' -f1 | grep -o '..$')
			fi
			number=$(echo "$p" | sed 's/.*(//; s/).*//; s/\..*//')
		else 
			year=$(echo "$p" | sed 's/.*_//;s/\.JPG//' | grep -o '..$')
			number="0"	
		fi

		[[ $oldId != $utid ]] && echo "ID mismatch for $oldId in file name $p. ($utid instead of $oldId)" 
		year=$(echo "$year" | sed 's/\.$//;s/\[$//;s/\]$//;')
	
		if [ $year -gt $startYear ] 
		then 
      y=$((year - startYear))
		else 
      y=0
		fi 
	
		[[ $number -lt 10 ]] && number="0$number"
		if [[ -L "$newId$y$month$day.$number.JPG" ]] 
		then 
			echo Trying to make symbolic link: "$newId$y$month$day.$number.JPG" to file "$zz$p" failed.
      echo Link by that name already exists here: 
      echo -e " $(ls -l $newId$y$month$day.$number.JPG) \n"
		else
			ln -s "../../$zz$p" "$newId$y$month$day.$number.JPG"
		fi
  done
  cd ../../
done
