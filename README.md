# ICPUTRD Image Platform

This repository is for the work on the web interface to annotate images.

* It is built on MEANJS stack https://github.com/meanjs/mean

* Example tools that may be relevant:
     1. Annotating image regions: http://annotorious.github.io/demos.html
     
     
* Backup database (do that periodically and before any server restarts/docker changes)   
     1. connect to a container that is linked to db
     2. DT=$(date +"%Y%m%d")
     3. mkdir -p $DT/mean-dev
     4. mongodump -h db:27017 --db mean-dev --out $DT
     
* Restore database     
     1. connect to a container that is linked to db (/data/DATE/mean-dev is where it was previously dumped)
     2. mongorestore -h db -d mean-dev /data/DATE/mean-dev
     3. if only one collection needs update, drop it and   mongoimport -h db -d mean-dev -c tags --type=csv  --mode=insert--headerline  tags.csv

* Process of porting the database
     1. use a python script named new_tags.py that is located in ~dlee97/ICPUTRD directory on da1 (also here) 
     2. connect to a container that is linked to dbNew (where dbNew is a copy of db)
     3. run "python3 new_tags.py" which will update all of the tags the script saves the newly named tags as a new collection named new_tags 
          - start the mongo shell in the container using "mongo --host=db"
          - run "db.tags.renameCollection("old_tags")"
          - then "db.new_tags.renameCollection("tags")"

# Setup server
- First step is to create a container on the server using the following command. Adjust ```[user_id]```, ```[port]``` and ```[container_name]```.  
```
docker run -d 
     -v /data/icputrd/arf/mean.js/public/2015:/opt/mean.js/public/2015 \ 
     -v /data/icputrd/arf/mean.js/public/2016:/opt/mean.js/public/2016 \
     -v /data/icputrd/arf/mean.js/public/2014:/opt/mean.js/public/2014 \
     -v /data/icputrd/arf/mean.js/public/2013:/opt/mean.js/public/2013 \
     -v /data/icputrd/arf/mean.js/public/2012:/opt/mean.js/public/2012 \
     -v /data/icputrd/arf/mean.js/public/2011:/opt/mean.js/public/2011 \
     -v /data/icputrd/arf/mean.js/public/2017:/opt/mean.js/public/2017 \
     -v /data/icputrd/arf/mean.js/public/sara_img:/opt/mean.js/public/sara_img \
     -v /data/icputrd/arf/mean.js/public/:/opt/mean.js/public/ \
     -v /data/icputrd/arf/mean.js/public/js:/opt/mean.js/public/js \
     -v /data/icputrd/arf/mean.js/public/css:/opt/mean.js/public/css \
     -v /home/[user_id]:/home/[user_id] -p [port]:22 \
     --name [container_name] \
     --link dbNew:dbNew \
     icputrd/platform /bin/startsvc.sh [user_id]    
```

- ```ssh da1p``` Then, append the user's ```id_rsa.pub``` to ```~/.ssh/authorized_keys``` for authorization. 


# Setup remote connection
- Generating ssh keys  
     - For Linux and Mac: ```ssh-keygen```. Once it completes, it should produces the ```id_rsa.pub``` and ```id_rsa``` files inside your ```~/.ssh/``` folder.  
     - For Windows: Please refer to [https://phoenixnap.com/kb/generate-ssh-key-windows-10](https://phoenixnap.com/kb/generate-ssh-key-windows-10)  
     After some experiments, we think the easiest way for Windows users is to use Powershell. You should also get the ```id_rsa.pub``` and ```id_rsa``` files inside your ```some_location/.ssh/``` folder (Note: ```some_location``` should be displayed at the end of the ```ssh-keygen``` process)  
     (Powershell approach) Create and copy code below to file ```icputrd.bat```; save it as ***ANSI format*** on Desktop/ for convenience. ([information about batch file in Windows 10](https://www.makeuseof.com/tag/write-simple-batch-bat-file/)) 
     **(Note:)** You may want to remove all comments, if cmd pops up and disappears; when you run the bat file. 
```
@echo off
set url="http://localhost:3000"
start chrome %url%
<# You could open with different browsers #>
<# start iexplore.exe %url% #>
<# start firefox.exe %url% #>
<# start microsoft-edge:%url% #>
<# Powershell.exe ssh da1p #>
Powershell.exe -WindowStyle Minimized ssh da1p
pause
```

- **Copy and paste the public key into [this form](https://docs.google.com/forms/d/e/1FAIpQLSe_2zBBTcuKAa1UdAIAZikvkjfXZBrzvvvFU-20A18t-FXcvw/viewform?usp=pp_url).**  
     Your ```id_rsa.pub``` is needed for setting up the connection. To see the public key, simply run ```cat ~/.ssh/id_rsa.pub```. 

- Set up your ```~/.ssh/config``` file so that you can login to one of the da servers without having to fully specify the server name each time. Simply copy the content below to ```~/.ssh/config```.  
```
Host *
  ForwardAgent yes
  
host da1p
    hostname da1.eecs.utk.edu
    identityfile ~/.ssh/id_rsa
    # identityfile C:\Users\Windows_user_name/.ssh/id_rsa // Windows user may have something like this
    user [user_id_on_server]
    Port [port]
    LocalForward 3000 127.0.0.1:3000
```

- Connect to ICPUTRD and creating an new account
     1. In your Terminal or cmd, type ```ssh da1p``` to connect
     2. Go to [http://localhost:3000/](http://localhost:3000/)
     3. Creating an new account