#!/bin/bash
## This script shoud be run in /opt/mean.js/public
index=1
ls -d 201[1-6]/*/Daily\ Photos/ |  while read zz; 
do	
  oldId=$(echo "$zz" | cut -d'/' -f2)
  startYear=$(echo "$zz" | cut -d'/' -f1 | grep -o '..$')
  echo $oldId
  newId=$(cat sara_corrected_mapping.txt | grep "$oldId" | cut -d " " -f 2)
  echo $oldId $newId
  mkdir -p sara_img/$newId
  cd sara_img/$newId
  index=$((index+1))
  
  ls "../../$zz" | grep -v "icon" | grep ".JPG" | while read p; 
  do
    utid=$(echo "$p" | sed 's/_.*//') 
    month=$(echo "$p" | cut -d'_' -f2)
    day=$(echo "$p" | cut -d'_' -f3)
    if echo "$p"  | grep '(' > /dev/null 
    then
        if echo "$p" | grep " " > /dev/null
	then 
		year=$(echo "$p" |  sed 's/.*_//' | cut -d ' ' -f1 | grep -o '..$')
	else
		year=$(echo "$p" | sed 's/.*_//' | cut -d '(' -f1 | grep -o '..$')
	fi
    number=$(echo "$p" | sed 's/.*(//; s/).*//; s/\..*//')
    else 
	 year=$(echo "$p" | sed 's/.*_//;s/\.JPG//' | grep -o '..$')
	 number="0"	
    fi
    
    [[ $oldId != $utid ]] && echo "ID mismatch for $oldId in file $p. $utid instead of $oldId" 
    year=$(echo "$year" | sed 's/\.$//;s/\[$//;s/\]$//;')
    if [ $year -gt $startYear ] 
    then 
	    y=$((year - startYear))
    else 
	    y=0
    fi 


    [[ $number -lt 10 ]] && number="0$number"

    ln -s "../../$zz$p" "$newId$y$month$day.$number.JPG"

    done
    cd ../../
done
