#!/bin/bash

index=1
ls -d 201[1-6]/*/Daily\ Photos/ |  while read zz; do	
  oldId=$(echo "$zz" | cut -d'/' -f2)
  startYear=$(echo "$zz" | cut -d'/' -f1 | grep -o '..$')
  newId=$(cat sara_corrected_mapping.txt | sed -n ''$index'p')
  echo $oldId $newId
  mkdir -p sara_img/$newId
  cd sara_img/$newId
  index=$((index+1))
  
  ls "../../$zz" | grep -v "icon" | grep ".JPG" | while read p; do
    utid=$(echo "$p" | sed 's/_.*//') 
    month=$(echo "$p" | cut -d'_' -f2)
    day=$(echo "$p" | cut -d'_' -f3)

		if echo "$p"  | grep '(' > /dev/null 
		then
			if echo "$p" | grep " " > /dev/null
			then 
				year=$(echo "$p" |  sed 's/.*_//' | cut -d ' ' -f1 | grep -o '..$')
			else
				year=$(echo "$p" | sed 's/.*_//' | cut -d '(' -f1 | grep -o '..$')
			fi
			number=$(echo "$p" | sed 's/.*(//; s/).*//; s/\..*//')
		else 
			year=$(echo "$p" | sed 's/.*_//;s/\.JPG//' | grep -o '..$')
			number="0"	
		fi
		
		[[ $oldId != $utid ]] && echo "ID mismatch for $oldId in file $p. $utid instead of $oldId" 
		year=$(echo "$year" | sed 's/\.$//;s/\[$//;s/\]$//;')
	
		if [ $year -gt $startYear ] 
			then y=$((year - startYear))
			else y=0
		fi 
	
		date=$y$month$day
		if [[ ! -d $date ]]
		then 
			mkdir $date
		fi
		cd $date

		[[ $number -lt 10 ]] && number="0$number"
		
		if [[ -L "$newId$y$month$day.$number.JPG" ]] 
		then 
			echo ../../../$zz$p $newId$y$month$day.$number.JPG $(ls -l $newId$y$month$day.$number.JPG)
		else 
			ln -s "../../../$zz$p" "$newId$y$month$day.$number.JPG"
		fi

		cd ../
  done
  cd ../../
done
