'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  TagLog = mongoose.model('TagLog'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create a TagLog
 */
exports.create = function (req, res) {
  console.log('create - ' + JSON.stringify(req.body));
  var taglog = new TagLog(req.body);
  taglog.user = req.user;
  taglog.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(taglog);
    }
  });
};

