'use strict';

/**
 * Module dependencies
 */
var taglogs = require('../controllers/taglogs.server.controller');

module.exports = function (app) {
  app.route('/api/taglogs').post(taglogs.create);
};
