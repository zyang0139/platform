(function () {
  'use strict';

  angular
    .module('tags')
    .controller('SearchController', SearchController);

  SearchController.$inject = ['$scope', 'Authentication'];

  function SearchController($scope, Authentication) {
    var vm = this;
    vm.authentication = Authentication;

  }
}());
