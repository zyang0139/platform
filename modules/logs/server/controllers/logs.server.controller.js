'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Log = mongoose.model('Log'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create a Log
 */
exports.create = function (req, res) {
  console.log('create - ' + JSON.stringify(req.body));
  var log = new Log(req.body);
  log.user = req.user;
  log.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(log);
    }
  });
};

