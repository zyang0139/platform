(function () {
  'use strict';

  angular
    .module('weather.services')
    .factory('WeatherService', WeatherService);

  WeatherService.$inject = ['$resource', '$log'];

  function WeatherService($resource, $log) {
    console.log('inside service');
    var Weather = $resource('/api/weather/', {
      weatherId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });

    return Weather;
  }

}());
