(function () {
  'use strict';

  angular
    .module('images')
    .controller('SearchImagesController', SearchImagesController);

  SearchImagesController.$inject = ['$scope', 'Authentication', 'SearchImagesService'];

  function SearchImagesController($scope, Authentication, SearchImagesService) {
    var vm = this;

    $scope.message = '';
    $scope.message2 = '';
    vm.authentication = Authentication;
    $scope.searchString = '';
    vm.simages = SearchImagesService.query();
    var result = [];
    var resultForSubsearch = [];
// //////////////////////////////////////////////////
// //////////////////SEARCH//////////////////////////
// //////////////////////////////////////////////////
    $scope.search = function() {
      if (!$scope.searchString) {
        console.log('What doesn\'t go here');
        return '';
      }
      result = null;
      result = [];
      var image = [];
      var imageCount = 0;
      var doubleCount = 0;
      $scope.searchString = $scope.searchString.toLowerCase();
      angular.forEach(vm.simages, function(item) {
        if (item.image.toLowerCase().indexOf($scope.searchString) !== -1) {
          imageCount++;
          image.push(item.image);
          var cat = item.image;
//          cat = cat.replace(/http:\/\/localhost:3000\/2015/, '');
          cat = cat.replace(/Daily Photo(s*)/, 'Daily Photos');
          cat = cat.replace(/JPG/, 'html');
          item.image = cat;
          var team = item;
          cat = cat.replace(/http:\/\/localhost:3000/, '');
          cat = cat.replace(/html/, 'JPG');
          team.pic = cat;
          result.push(team);
        }

      });
      vm.images = result;
      resultForSubsearch = result;
      vm.count = imageCount;
      vm.images = image;
      $scope.searchString = '';
// The return is not really needed nor is it needed above in the "if"
// statement
      return result;
    };
// refineSearch //////////////////////////////////////////////
    $scope.refineSearch = function() {
      var imageCount = 0;
      $scope.searchString = $scope.searchString.toLowerCase();
      var small = $scope.searchString;
      var result2 = [];
      angular.forEach(resultForSubsearch, function(item) {
        if (item.image.toLowerCase().indexOf($scope.searchString) !== -1) {
          result2.push(item);
          imageCount++;
        }
      });
      vm.images = result2;
      resultForSubsearch = result2;
      vm.count = imageCount;
      return 'img';
    };
// clear /////////////////////////////////////////////////////
    $scope.clear = function() {
      $scope.searchString = '';
      resultForSubsearch = null;
      result = null;
      vm.count = null;
      vm.images = null;
    };
  }
}());
