'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Tag Schema
 */
var TagSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  tag: {
    type: String,
    default: '',
    trim: true,
    required: 'Tag cannot be blank'
  },
  image: {
    type: String,
    default: ''
  },
  utid: {
    type: String,
    default: ''
  },
  location: {
    type: String,
    default: ''
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});


var createCompoundIndex = function(db, callback) {
//  Get the documents collection
  var collection = db.collection('tags');
  // Create the index
  collection.createIndex(
    { '$**': 'text' }, function(err, result) {
      console.log(result);
      callback(result);
    });
};
mongoose.model('Tag', TagSchema);
