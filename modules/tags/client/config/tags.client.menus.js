(function () {
  'use strict';

  angular
    .module('tags')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(menuService) {
    menuService.addMenuItem('topbar', {
      title: 'Tags',
      state: 'tags',
      type: 'dropdown',
      roles: ['*']
    });

    // Add the dropdown list item
    menuService.addSubMenuItem('topbar', 'tags', {
      // title: 'List Tags',
      state: 'tags.list',
      roles: ['*']
    });
    menuService.addSubMenuItem('topbar', 'tags', {
      title: 'Search Tags',
      state: 'tags.search',
      roles: ['*']
    });
  }
}());
