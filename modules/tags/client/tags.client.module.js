(function (app) {
  'use strict';

  app.registerModule('tags', ['core']);// The core module is required for special route handling; see /core/client/config/core.client.routes
  app.registerModule('tags.admin', ['core.admin']);
  app.registerModule('tags.admin.routes', ['core.admin.routes']);
  app.registerModule('tags.services');
  app.registerModule('tags.routes', ['ui.router', 'core.routes', 'tags.services']);
}(ApplicationConfiguration));
