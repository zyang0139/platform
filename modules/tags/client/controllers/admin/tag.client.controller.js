﻿(function () {
  'use strict';

  angular
    .module('tags.admin')
    .controller('TagsAdminController', TagsAdminController);

  TagsAdminController.$inject = ['$scope', '$state', '$window', 'tagResolve', 'Authentication', 'Notification'];

  function TagsAdminController($scope, $state, $window, tag, Authentication, Notification) {
    var vm = this;

    vm.tag = tag;
    vm.authentication = Authentication;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    // Remove existing Tag
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.tag.$remove(function() {
          $state.go('admin.tags.list');
          Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Tag deleted successfully!' });
        });
      }
    }

    // Save Tag
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.tagForm');
        return false;
      }

      // Create a new tag, or update the current instance
      vm.tag.createOrUpdate()
        .then(successCallback)
        .catch(errorCallback);

      function successCallback(res) {
        $state.go('admin.tags.list'); // should we send the User to the list or the updated Tag's view?
        Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Tag saved successfully!' });
      }

      function errorCallback(res) {
        Notification.error({ message: res.data.message, title: '<i class="glyphicon glyphicon-remove"></i> Tag save error!' });
      }
    }
  }
}());
