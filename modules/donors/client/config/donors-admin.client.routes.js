﻿(function () {
  'use strict';

  angular
    .module('donors.admin.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('admin.donors', {
        abstract: true,
        url: '/donors',
        template: '<ui-view/>'
      })
      .state('admin.donors.list', {
        url: '',
        templateUrl: '/modules/donors/client/views/admin/list-donors.client.view.html',
        controller: 'DonorsAdminListController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.donors.create', {
        url: '/create',
        templateUrl: '/modules/donors/client/views/admin/form-donor.client.view.html',
        controller: 'DonorsAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        },
        resolve: {
          donorResolve: newDonor
        }
      })
      .state('admin.donors.edit', {
        url: '/:donorId/edit',
        templateUrl: '/modules/donors/client/views/admin/form-donor.client.view.html',
        controller: 'DonorsAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        },
        resolve: {
          donorResolve: getDonor
        }
      });
  }

  getDonor.$inject = ['$stateParams', 'DonorsService'];

  function getDonor($stateParams, DonorsService) {
    return DonorsService.get({
      donorId: $stateParams.donorId
    }).$promise;
  }

  newDonor.$inject = ['DonorsService'];

  function newDonor(DonorsService) {
    return new DonorsService();
  }
}());
