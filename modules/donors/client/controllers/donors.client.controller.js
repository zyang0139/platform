(function () {
  'use strict';

  angular
    .module('donors')
    .controller('DonorsController', DonorsController);

  DonorsController.$inject = ['$scope', 'donorResolve', 'Authentication'];

  function DonorsController($scope, donor, Authentication) {
    var vm = this;
    vm.donor = donor;
    vm.authentication = Authentication;

  }
}());
