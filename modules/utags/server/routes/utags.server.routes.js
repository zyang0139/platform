'use strict';

/**
 * Module dependencies
 */
var utagsPolicy = require('../policies/utags.server.policy'),
  utags = require('../controllers/utags.server.controller');

module.exports = function (app) {
  // Utags collection routes
  app.route('/api/utags').all(utagsPolicy.isAllowed).get(utags.list).post(utags.create);
  // Single utag routes
  app.route('/api/utags/:utagId').all(utagsPolicy.isAllowed).get(utags.read).put(utags.update).delete(utags.delete);
  // Finish by binding the utag middleware
  app.param('utagId', utags.utagByID);
  // New route for image only utags
  app.route('/api/utags/first/:utgs').all(utagsPolicy.isAllowed).get(utags.readImage);
  app.param('utgs', utags.imageID);
};
