(function () {
  'use strict';

  angular
    .module('utags.services')
    .factory('UtagsService', UtagsService);

  UtagsService.$inject = ['$resource', '$log'];

  function UtagsService($resource, $log) {
    console.log('inside service');
    var Utag = $resource('/api/utags/:utagId', {
      utagId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
    return Utag;
  }
}());
