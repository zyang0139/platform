(function () {
  'use strict';

  angular
    .module('utags.services')
    .factory('utagFactory', utagFactory);

  utagFactory.$inject = ['$resource', '$log', '$http'];

  function utagFactory($resource, $log, $http) {
    var url = 'api/utags';
    var pop = 'http://localhost:3000/api/utags';
//    console.log('aaaaa', url);

    function list() {
      return $http.get(pop)
        .then(function(response) {
          return response.data;
        });
    }
// ========================================================
    function find(uTagArray, demos, sString) {
      var image = [];
      var demoStuff = [];
      var tagCount = 0;
      var retVal = {};
      console.log(demos);
      angular.forEach(uTagArray, function(item) {
        if (item.tag.toLowerCase().indexOf(sString) !== -1) {
          console.log(item.tag);
          tagCount++;
          var demoObj = {};
          var cat = item.image;
          cat = cat.replace(/Daily Photo(s*)/, 'Daily Photos');
          cat = cat.replace(/JPG/, 'html');
          item.image = cat;
          cat = cat.replace(/http:\/\/localhost:3000/, '');
          cat = cat.replace(/html/, 'JPG');
          item.pic = cat;
          image.push(item);
        }
        angular.forEach(demos, function(ditem) {
          if (item.utid === ditem.utid) demoStuff.push(ditem);
        });
      });
      console.log('here here here');
      retVal.demo = demoStuff;
      retVal.image = image;
      retVal.count = tagCount;
      return retVal;
    }
    function add() {
      console.log('add here');
      return $http.get(url);
    }
    return {
      add: add,
      find: find,
      list: list
    };

  }
}());
