(function () {
  'use strict';

  angular
    .module('utags')
    .controller('SearchUtagsController', SearchUtagsController);
  SearchUtagsController.$inject = ['$scope', 'Authentication', 'UtagsService', 'utagFactory', 'demographyFactory', 'DemographyService', 'TagsService'];

  function SearchUtagsController($scope, Authentication, UtagsService, utagFactory, demographyFactory, DemographyService, TagsService) {
    var vm = this;

    $scope.message = '';
    $scope.message2 = '';
    vm.authentication = Authentication;
    $scope.searchString = '';
    var demos;
// Needs to be a promise
    demographyFactory.list()
      .then(function(data) {
        demos = data;
      });
    var sutags = UtagsService.query();
    console.log(demos);
    var result = [];
    var resultForSubsearch = [];
// //////////////////////////////////////////////////
// //////////////////SEARCH//////////////////////////
// //////////////////////////////////////////////////
    $scope.search = function() {
      if (!$scope.searchString) {
        return '';
      }
      var image = [];
      var utagCount = 0;
      var doubleCount = 0;
      var result = utagFactory.find(sutags, demos, $scope.searchString.toLowerCase());
      vm.utags = result.image;
      vm.utagsDemo = result.demo;
      $scope.utags1 = result.image;
      $scope.utagsDemo1 = result.demo;
      resultForSubsearch = result;
      vm.count = result.count;
      $scope.searchString = '';
    };
// refineSearch //////////////////////////////////////////////
    $scope.refineSearch = function() {
      var utagCount = 0;
      $scope.searchString = $scope.searchString.toLowerCase();
      var small = $scope.searchString;
      var result2 = [];
      angular.forEach(resultForSubsearch, function(item) {
        if (item.utag.toLowerCase().indexOf($scope.searchString) !== -1) {
          result2.push(item);
          utagCount++;
        }
      });
      vm.utags = result2;
      resultForSubsearch = result2;
      vm.count = utagCount;
      return 'img';
    };
// clear /////////////////////////////////////////////////////
    $scope.clear = function() {
      $scope.searchString = '';
      resultForSubsearch = null;
      result = null;
      vm.count = null;
      vm.utags = null;
    };
  }
}());
