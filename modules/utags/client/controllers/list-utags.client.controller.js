(function () {
  'use strict';

  angular
    .module('utags')
    .controller('UtagsListController', UtagsListController);

  UtagsListController.$inject = ['UtagsService'];

  function UtagsListController(UtagsService) {
    var vm = this;

    vm.utags = UtagsService.query();
  }
}());
