(function () {
  'use strict';

  angular
    .module('utags.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('utags', {
        abstract: true,
        url: '/utags',
        template: '<ui-view/>'
      })
      .state('utags.list', {
        url: '',
        templateUrl: '/modules/utags/client/views/list-utags.client.view.html',
        controller: 'UtagsListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Utags List'
        }
      })
      .state('utags.search', {
        url: '/search',
        templateUrl: '/modules/utags/client/views/search-utags.client.view.html',
        controller: 'SearchUtagsController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Utags Search'
        }
      })
      .state('utags.view', {
        url: '/:utagId',
        templateUrl: '/modules/utags/client/views/view-utag.client.view.html',
        controller: 'UtagsController',
        controllerAs: 'vm',
        resolve: {
          utagResolve: getUtag
        },
        data: {
          pageTitle: 'Utag {{ utagResolve.title }}'
        }
      });
  }

  getUtag.$inject = ['$stateParams', 'UtagsService'];

  function getUtag($stateParams, UtagsService) {
    return UtagsService.get({
      utagId: $stateParams.utagId
    }).$promise;
  }

  function searchUtag($stateParams, SearchUtagsService) {
    return SearchUtagsService.get({
      utagId: $stateParams.utagId
    }).$promise;
  }


}());
