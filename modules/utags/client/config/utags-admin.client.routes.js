﻿(function () {
  'use strict';

  angular
    .module('utags.admin.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('admin.utags', {
        abstract: true,
        url: '/utags',
        template: '<ui-view/>'
      })
      .state('admin.utags.list', {
        url: '',
        templateUrl: '/modules/utags/client/views/admin/list-utags.client.view.html',
        controller: 'UtagsAdminListController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.utags.create', {
        url: '/create',
        templateUrl: '/modules/utags/client/views/admin/form-utag.client.view.html',
        controller: 'UtagsAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        },
        resolve: {
          utagResolve: newUtag
        }
      })
      .state('admin.utags.edit', {
        url: '/:utagId/edit',
        templateUrl: '/modules/utags/client/views/admin/form-utag.client.view.html',
        controller: 'UtagsAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        },
        resolve: {
          utagResolve: getUtag
        }
      });
  }

  getUtag.$inject = ['$stateParams', 'UtagsService'];

  function getUtag($stateParams, UtagsService) {
    return UtagsService.get({
      utagId: $stateParams.utagId
    }).$promise;
  }

  newUtag.$inject = ['UtagsService'];

  function newUtag(UtagsService) {
    return new UtagsService();
  }
}());
