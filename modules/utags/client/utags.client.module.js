(function (app) {
  'use strict';

  app.registerModule('utags', ['core']);// The core module is required for special route handling; see /core/client/config/core.client.routes
  app.registerModule('utags.admin', ['core.admin']);
  app.registerModule('utags.admin.routes', ['core.admin.routes']);
  app.registerModule('utags.services');
  app.registerModule('utags.routes', ['ui.router', 'core.routes', 'utags.services']);
}(ApplicationConfiguration));
