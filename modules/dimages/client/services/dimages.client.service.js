(function () {
  'use strict';

  angular
    .module('dimages.services')
    .factory('DimagesService', DimagesService);

  DimagesService.$inject = ['$resource', '$log'];

  function DimagesService($resource, $log) {
    console.log('inside service');
    var Dimage = $resource('/api/dimages/:dimageId', {
      dimageId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });

    return Dimage;
  }

}());

