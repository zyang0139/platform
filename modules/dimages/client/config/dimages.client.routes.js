(function () {
  'use strict';

  angular
    .module('dimages.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('dimages', {
        abstract: true,
        url: '/dimages',
        template: '<ui-view/>'
      })
      .state('dimages.list', {
        url: '',
        templateUrl: '/modules/dimages/client/views/list-dimages.client.view.html',
        controller: 'DimagesListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Dimages List'
        }
      })
      .state('dimages.listU', {
        url: '/listimage',
        templateUrl: '/modules/dimages/client/views/search-dimages.client.view.html',
        controller: 'SearchDimagesController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'ID Image List'
        }
      })
      .state('dimages.search', {
        url: '/search',
        templateUrl: '/modules/dimages/client/views/search-dimages.client.view.html',
        controller: 'SearchDimagesController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Dimages Search'
        }
      });
  }

}());
