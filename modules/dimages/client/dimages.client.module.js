(function (app) {
  'use strict';

  app.registerModule('dimages', ['core']);// The core module is required for special route handling; see /core/client/config/core.client.routes
  app.registerModule('dimages.admin', ['core.admin']);
  app.registerModule('dimages.admin.routes', ['core.admin.routes']);
  app.registerModule('dimages.services');
  app.registerModule('dimages.routes', ['ui.router', 'core.routes', 'dimages.services']);
}(ApplicationConfiguration));
