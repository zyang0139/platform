'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Donor Schema
 */
var DonorsSchema = new Schema({

  _id: {
    type: String,
    default: ''
  },

  icon: {
    type: String,
    default: ''
  },
  totalPhotos: {
    type: Number,
    default: ''
  },
  numDays: {
    type: Number,
    default: ''
  },
  days: {
    type: [{ date: { type: String, default: '' },
      icon: { type: String, default: '' },
      numPhotos: { type: Number, default: 0 },
      images: { type: [{ path: { type: String, default: '' }, num: { type: String, default: '' }, icon: { type: String, default: '' }, html: { type: String, default: '' } }] } }],
    default: []
  }
});


var createCompoundIndex = function(db, callback) {
  // Get the documents collection
  var collection = db.collection('donors');
  // Create the index
  collection.createIndex(
    { '_id': 1, 'days': 1 }, function(err, result) {
      console.log(result);
      callback(result);
    });
};
mongoose.model('Donors', DonorsSchema);
