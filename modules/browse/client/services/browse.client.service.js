(function () {
  'use strict';

  angular
    .module('browse.services')
    .factory('BrowseService', BrowseService)
    .factory('DaysService', DaysService)
    .factory('ImagesService', ImagesService)
    .factory('ImageService', ImageService);

  BrowseService.$inject = ['$resource', '$log'];
  DaysService.$inject = ['$resource', '$log'];
  ImagesService.$inject = ['$resource', '$log'];
  ImageService.$inject = ['$resource', '$log'];

  function BrowseService($resource, $log) {
    console.log('browse service');
    var Browse = $resource('/api/browse', {
      browseId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
    console.log('Browse');
    return Browse;
  }

  function DaysService($resource, $log) {
    console.log('days service');
    var Days = $resource('/api/browse/:browseId', {
      date: '@days.date'
    }, {
      update: {
        method: 'PUT'
      }
    });
    console.log('Days');
    return Days;
  }

  function ImagesService($resource, $log) {
    console.log('images service');
    var Images = $resource('/api/browse/:browseId/:date', {
      num: '@days.images.num'
    }, {
      update: {
        method: 'PUT'
      }
    });
    console.log('Images');
    return Images;
  }

  function ImageService($resource, $log) {
    console.log('image service');
    var Image = $resource('/api/browse/:browseId/:date/:num', {
      path: '@days.images.path'
    }, {
      update: {
        method: 'PUT'
      }
    });
    console.log('Image');
    return Image;
  }

}());
