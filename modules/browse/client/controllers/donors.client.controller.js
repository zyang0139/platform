(function () {
  'use strict';

  angular
    .module('browse')
    .controller('BrowseListController', BrowseListController);

  BrowseListController.$inject = ['BrowseService', '$scope'];

  function BrowseListController(BrowseService, $scope) {
    var vm = this;
    vm.browse = BrowseService.query();
    /*
    $scope.filterItems = {
      'numDays': true
    };
    $scope.testFilter = function(browse) {
      console.log(browse._id);
      return $scope.filterItems[browse.numDays];
    };
    */
    console.log(vm.browse);
  }
}());
