(function (app) {
  'use strict';

  app.registerModule('demography', ['core']);// The core module is required for special route handling; see /core/client/config/core.client.routes
  app.registerModule('demography.admin', ['core.admin']);
  app.registerModule('demography.admin.routes', ['core.admin.routes']);
  app.registerModule('jtt_openweathermap');
  app.registerModule('ui.openseadragon');
  app.registerModule('demography.services');
  app.registerModule('tags.services');
  app.registerModule('demography.routes', ['ui.router', 'core.routes', 'demography.services', 'tags.services']);
}(ApplicationConfiguration));
