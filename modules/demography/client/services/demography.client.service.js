(function () {
  'use strict';

  angular
    .module('demography.services')
    .factory('DemographyService', DemographyService);

  DemographyService.$inject = ['$resource', '$log'];

  function DemographyService($resource, $log) {
    var Demography = $resource('/demography/:demographyId', {
      demographyId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });

    angular.extend(Demography.prototype, {
      createOrUpdate: function () {
        var demography = this;
        return createOrUpdate(demography);
      }
    });

    return Demography;

    function createOrUpdate(demography) {
      if (demography._id) {
        return demography.$update(onSuccess, onError);
      } else {
        return demography.$save(onSuccess, onError);
      }

      // Handle successful response
      function onSuccess(demography) {
        // Any required internal processing from inside the service, goes here.
      }

      // Handle error response
      function onError(errorResponse) {
        var error = errorResponse.data;
        // Handle error internally
        handleError(error);
      }
    }

    function handleError(error) {
      // Log error
      $log.error(error);
    }
  }
}());
