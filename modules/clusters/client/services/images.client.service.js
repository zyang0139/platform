(function () {
  'use strict';

  angular
    .module('clusters.services')
    .factory('ClusterImagesService', ClusterImagesService);

  ClusterImagesService.$inject = ['$resource', '$log'];

  function ClusterImagesService($resource, $log) {
    console.log('cluster images service');
    var images = $resource('/api/clusters/:label', {}, {
      update: {
        method: 'PUT'
      }
    });
    console.log('images');
    return images;
  }
}());
