(function (app) {
  'use strict';

  // The core module is required for special route handling;
  // see /core/client/config/core.client.routes
  app.registerModule('clusters', ['core']);
  app.registerModule('clusters.services');
  app.registerModule('clusters.routes', ['ui.router', 'core.routes', 'clusters.services']);
}(ApplicationConfiguration));
