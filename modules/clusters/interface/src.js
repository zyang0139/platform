var arr = [];

function addToList(_src, id) {
	if(!arr.includes(_src)) {
		var path = _src + "\n"
		arr.push(_src);
	}
	document.getElementById(id).border="8";
    document.getElementById(id).style.borderColor = "red";
	console.log(id);
	console.log(arr);
}

// Function to download data to a file
function download(data, filename, type) {
	var file = new Blob([data], {type: type});
		if (window.navigator.msSaveOrOpenBlob) // IE10+
        window.navigator.msSaveOrOpenBlob(file, filename);
    else { // Others
        var a = document.createElement("a"),
                url = URL.createObjectURL(file);
        a.href = url;
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        setTimeout(function() {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);  
        }, 0); 
    }
}

document.addEventListener('DOMContentLoaded', function() {
    document.getElementById('set-cluster-name-btn').addEventListener('click', function(){
        var label = document.querySelector('.cluster-name').value;
        var pos = document.location.href.lastIndexOf('/');
        var cluster_name = document.location.href.substr(pos + 1);

        let images = document.querySelectorAll("img");
        images.forEach(img => {
            let path = img.src.match(/img\/.*/g);
            if (path == null) {
                return;
            }
            path = path[0].replace('.icon', '');
            $.post('http://localhost:3000/api/clusters/', {
                'label': label,
                'cluster_name': cluster_name,
                'image': path
            }, function(res){
                console.log(res); 
                document.getElementById("set-cluster-name-btn").style.color = "red";
            });
        });
    });
});
